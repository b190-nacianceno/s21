console.log("hello everyone!");


let cities = ["Tokyo", "Manila", "Seoul", "Jakarta", "Sim"];
console.log(cities);
// to check for the number of elements in an array
console.log(cities.length);
// to check the last index of the array
console.log(cities.length-1);
// check the last element thorugh the index
console.log(cities[cities.length-1]);

// in cases of blank arrays
blankArr = [];
// returns 0
console.log(blankArr.length);
// returns undefined
console.log(blankArr[blankArr.length]);

console.log(cities);
// decrement operator can be used to delete the last element of the array
cities.length--;
// cities.length = cities.length -1
console.log(cities);

let lakersLegends = ["Kobe","Shaq","Magic","Kareem","LeBron"];
console.log(lakersLegends);

lakersLegends.length++;
console.log(lakersLegends);

lakersLegends[5] = "Pau Gasol";
console.log(lakersLegends);

// use the .length to directly add elements at the end of an array
lakersLegends[lakersLegends.length] = "Anthony Davis";
console.log(lakersLegends);


let numArray = [5,12,30,46,40];

for(let index = 0; index < numArray.length; index++){
    if (numArray[index]%5 ===0){
        console.log(numArray[index] + " is divisible by 5");
    } else {
        console.log(numArray[index] + " is not divisible by 5");
    }
}
// log each of the elements 
for (let index = 0; index < numArray.length; index++) {
    console.log(numArray[index])

}

// log the reverse the order of the elements

for (let index = numArray.length; index >= 0; index--) {
    console.log(numArray[index])
};

// Multidimensional arrays (sometimes called nested arrays)
/* 
- useful for storing complex data structures, a practical application of this is to help visualize/create real world objects
-though useful in a number of cases, creating complex arrays is not always recommended - nakakalito
*/
let chessBoard = [
["a1","b1","c1","d1","e1"],
["a2","b2","c2","d2","e2"],
["a3","b3","c3","d3","e3"],
["a4","b4","c4","d4","e4"],
["a5","b5","c5","d5","e5"],
];

console.log(chessBoard);

// accessing a multidimensional array 
console.log(chessBoard[2][3]);
