// console.log("hello Everyone");

var grades = [98.5, 94.3, 89.2, 90.1];
console.log(grades);

/* 
Arrays are used to store multiple related values in a single variable. They are declared using square brackets [] also known as "Array Literals"
Arrays also provide access to a number of functions/methods that help in achieving tasks that we can perform on the elements inside the array.

Arrays are used to store numerous amounts of data to manipulate in order to perform methods

Methods are used, similar to functions associated with objects

Arrays are also referred to as objects which is another data type. The only difference between the tow is that arrays contain information in a form of list, but an object uses "properties" and "values" in its elements.

    SYNTAX:
    let/const arrayName = [elementA, elementB, elementC,... elementN]
*/

// it is important that we store RELATED values inside an array so that the variable can live up to it designated value; recommended also same datatypes in an array
let computerBrands = ["Lenovo", "Dell", "Asus", "HP", "CDR-King", "Acer", "Mac"];

let mixedArray = [12, "Asus", null, undefined];
console.log(mixedArray);


// Reassigning of array values
console.log("Array before reassigning");
console.log(mixedArray);
// accessing the elements require the arrayName + index, enclosed in square brackets
// this can also be done in const arrays
mixedArray[0] = "hello world";
console.log("Array after reassigning");
console.log(mixedArray);


// SECTION - Reading from Arrays
/* 
Accessing array elements is one of the more common tasks that we do with an array. This can be done through the use of array indeces
    index-term used to declare the position of an element in an arry.
    In JS, the first elemetn is associated with number 0, incrementing as the number of elements increase.

    SYNTAX:
        ArrayName[index];
*/
console.log(grades);
console.log(grades[0]);
console.log(computerBrands[3]);
console.log(computerBrands[6]);
// how we can get the number of elements in an array
console.log(computerBrands.length);
// accessing an index that is not existing would return undefined because this means that the element does not exist

console.log(computerBrands[7]);
// One of the common use for the .length property
if (computerBrands.length > 5){
    console.log("We have too many suppliers. Please coordinate with the operations manager.")
}

// Since the first element of an array starts at 0, subtracting 1 from the length will offset the value by one allowing us to get the last index of the array, incase we forgot the number of its element
let lastElementIndex = computerBrands.length -1;
console.log(computerBrands[lastElementIndex]);

// Section - Array Methods
// methods are similar to functions, these array methods can be done in array and objects alon
// Mutator Methods


let fruits = ["Apple", "Mango", "Rambutan", "Lanzones", "Durian"];
console.log(fruits);
// push() - adds an element at the end of an array

// SYNTAX: arrayName.push()
let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);

console.log("Mutated Array from Push Method:");
console.log(fruits);

// frust[6] = "Orange"
// console.log(fruits);
// code above can also be written liek this:

fruits.push("Orange");
console.log("Mutated Array from Push Method:");
console.log(fruits);

// pop() - removes an element from the end of an array
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated Array from Pop Method:");
console.log(fruits);

// unshift() - adds elements at the start of an array
fruits.unshift("Lime", "Banana");
console.log("Mutated Array from Unshift Method: ");
console.log(fruits);

// shift() - removes an element from the start of an array

let fruitsRemoved = fruits.shift();
console.log("Mutated Array from Shift Method:");
console.log(fruits);

// splice  - simultaneously removes and adds elements from the specified index

// SYNTAX: arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
fruits.splice(2,2, "Lime", "CHERRY", "Strawberry");
console.log("Mutated Array from Splice Method:");
console.log(fruits);

// sort - rearrnages the elements in an array in alphanumeric order

// SYNTAX: arrayName.sort();
fruits.sort();
console.log("Mutated Array from Sort Method:");
console.log(fruits);


// reverse- reverses the order of the array
// SYNTAX: arrayName.reverse();
fruits.reverse();
console.log("Mutated Array from Reverse Method:");
console.log(fruits);

// Non-mutator methods - functions that do not modify the original array
// -these methods do not manipulate the elements inside the array even tho they are performing tasks such as returning elements from an array and combining them with other arrays and printing the output

// indexOf() -returns the index of the first matching element found in an array
// -the search process will start from the first elemetn down to the last
// if you search for non-existent elemetn -1 will print
// SYNTAX: arrayName.indexOf(searchValue);

let countries = ["PH","RUS","CH","JPN", "USA","Kor","AU", "CAN","PH"]
let firstIndex = countries.indexOf("PH");
console.log("Results of indexOf: " + firstIndex);

// lastIndexOf() - starts the search proceess from the last element down to the first
// SYNTAX: arrayName.lastIndexOf(searchValue);

let lastIndex = countries.lastIndexOf("PH");
console.log("Results fo lastIndexOf: " + lastIndex);

// slice - copies a part of the array and stores it and returns a new array
// SYNTAX: arrayName.slice(startingIndex);
// arrayName.slice(startingIndex, indexofLastElementIntheString);
// The slice() method selects from a given start, up to a (not inclusive) given end.


let slicArrayA = countries.slice(2);
console.log("Result of Slice: " + slicArrayA);

let slicArrayB = countries.slice(4,7);
console.log("Result of Slice: " + slicArrayB);

// negative in slice, used to slice off elements starting from the end of the array
let slicArrayC = countries.slice(-3);
console.log("Result of Slice: " + slicArrayC);

// toString() - returns a new array as a string separated by commas
// SYNTAX: arrayName.toString();
let stringArray = countries.toString();
console.log("Results of toString: " + stringArray);
console.log(countries)

// concat() - used to combine two arrays and returns the combined result
// SYNTAX: arrayA.concat(arrayB);
let tasksA = ["drink HTML", "eat JavaScript"];
let tasksB = ["inhale CSS", "breathe SASS"];
let tasksC = ["get GIT", "be node"];
let tasks = tasksA.concat(tasksB);
console.log("Result of concat: ");
console.log(tasks);

// combining multiple arrays using concat

let allTasks = tasksA.concat(tasksB,tasksC);
console.log("Result of concat: ");
console.log(allTasks);

// join() - returns an array as string separated by specified string separator

// SYNTAX: arrayName.join("stringSeparator");


let users = ["John","Jane","Joe","Jobert","Julius"];
console.log(users.join());
console.log(users.join(" "));
console.log(users.join("-"));


// Iterator Methods - loops designed to perform repetitive tasks on arrays
// useful for manipulating array data reuslting in complex tasks

// forEach - similar to a for loop that loops thru all elements
// variable names for arrays are usually written in plural form of the data stored in an array
// it's common practice to use the singular form of the array content for parameter names used in array loops
// array iterations normally work with a function supplied as an argument
// how this function works is by performing tasks that are predefined within the array's method
// SYNTAX: arrayName.forEach(function(individualElement){
// statements
// })
allTasks.forEach(function(task){
	console.log(task);
})

// forEach with conditional statements
let filteredTasks = [];

allTasks.forEach(function(task){
    if (task.length > 10){
        // we stored the filtered elements inside another variable to avoid confusion should we need the original array intact
        filteredTasks.push(task);
    }
})

console.log("Result of forEach: ");
console.log(filteredTasks);


// map - also iterates on ech element and RETURNS a new array with different values depending on the result fo the function's opertion.
// this is useful for performing tasks where mutating/changing the lements are required
// SYNTAX: let/const resultArray = arrayName.map(function(inidivualElement){RETURN STATEMENT
// })
let numbers = [1,2,3,4,5];

let numbersMap = numbers.map(function(number){
    // unlike forEach, return statemetn is needed in map method to create value that is stored in another array
    return number*number
})
console.log ("Result of map: ");
console.log(numbersMap);

// every() - checks if all elements pass the condition
// returns a boolean data type depending if all elements meet the coniditon (true/false)
// SYNTAX: let/const result = arrayName.every(function(individualElement){
    // return expresssion/condition
// }) 
let allValid = numbers.every(function(number){
    return (number<3);
})

console.log("Result of every: ");
console.log(allValid);

// some() - checks if atleast one element in the array passes the given condition
// also returns a boolean depending on the result of the function
// SYNTAXL: let/const resultName = arrayName.some(function(individualElement){return expression/condition})
let someValid = numbers.some(function(number){
    return (number<2);
})

console.log ("Result of some:");
console.log(someValid);
console.log(numbers);


// filter - filter and returns the array of elements that meet the condition
// returns empty array if no elements meet the condition
// useful for filtering array elements with a given condition and shortens the syntax compared to using other array iteration method such as forEach + if statement + push in an earlier example
    // it is important that we make our work as efficient as possible
    // SYNTAX: let/const resultName = arrayName.filter(function(individualElement){return expression/condition})

let filterValid = numbers.filter(function(number){
    return (number<3);
})

console.log ("Result of filter:");
console.log(filterValid);
console.log(numbers);